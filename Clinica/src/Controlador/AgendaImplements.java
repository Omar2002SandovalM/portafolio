/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.AgendaPojo;
import Modelo.IDaofindById;
import Modelo.IOCollections;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import javax.swing.JOptionPane;

/**
 *
 * @author Lenovo
 */
public class AgendaImplements implements IDaofindById{
    
    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 900;
    private Gson gson;

    public AgendaImplements() {
        gson = new Gson();
    }

    
    private void open() throws FileNotFoundException, IOException{
        fHead = new File("Agenda.head");
        fData = new File("Agenda.data");
        
        if(!fHead.exists()){
            fHead.createNewFile();
            rafHead = new RandomAccessFile(fHead, "rw");
            rafData = new RandomAccessFile(fData, "rw");
            rafHead.seek(0);
            rafHead.writeInt(0);//Valores de n
            rafHead.writeInt(0);//Valores de k
        } else {
            rafHead = new RandomAccessFile(fHead, "rw");
            rafData = new RandomAccessFile(fData, "rw");
        }
    }
    
    private void close() throws IOException{
        if(rafHead != null){
            rafHead.close();
        }
        
        if(rafData != null){
            rafData.close();
        }
    }
    
    private Field getField(String fieldName) throws NoSuchFieldException{
        Field field = AgendaPojo.class.getField(fieldName);
        return field;
    }

    @Override
    public AgendaPojo findById(int id) throws IOException {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();
        
        AgendaPojo a;
        if(id > k || id <= 0){
            //JOptionPane.showMessageDialog(null, "Id Invalido"+JOptionPane.ERROR_MESSAGE);
            return null;
        }
        
        int index = IOCollections.binarySearch(rafHead, (int) id, 0, n-1);
        long posHead = 8 + index * 4;
        rafHead.seek(posHead);
        int code = rafHead.readInt();
        
        if(code != id){
            //JOptionPane.showMessageDialog(null, "Id Invalido"+JOptionPane.ERROR_MESSAGE);
             return null;
        }
        
        long posData = (id - 1) *SIZE;
        rafData.seek(posData);
        
        String jsonAgenda = rafData.readUTF();
        
        if(jsonAgenda.substring(0,1).equals("*")){
            JOptionPane.showMessageDialog(null, "La informacion ha sido Eliminada");
            return null;
        }
        else{
            a = gson.fromJson(jsonAgenda,AgendaPojo.class); 
        }
        close();
        return  a;
    }

    @Override
    public AgendaPojo find(Comparator<AgendaPojo> fun) throws IOException {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        AgendaPojo Agenda = null;
        return Agenda;
    }

    @Override
    public List<AgendaPojo> any(Predicate<AgendaPojo> fun) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create(AgendaPojo t) throws IOException {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafData.readInt();
        
        long posData = k *SIZE;
        
        t.setId(k+1);
        String jsonAgenda = gson.toJson(t);
        
        rafData.seek(posData);
        rafData.writeUTF(jsonAgenda);
        
        long posHeader = 8 + n*4;
        
        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(++k);
        
        rafHead.seek(posHeader);
        rafHead.writeInt(k);
        close();
    }

    @Override
    public int update(AgendaPojo t) throws IOException {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();
        
        long datapos = (k -1)*SIZE;
        rafData.seek(datapos);
        
        String jsonAgenda = gson.toJson(t);
        
        rafData.writeUTF(jsonAgenda);
        
        close();
        return k;
    }

    @Override
    public boolean delete(int id) throws IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();
        
        if(id < 1 || id > k || id == 0){
            return false;
        }
        
        int code = IOCollections.binarySearch(rafHead, (int) id, 0, n-1);
        
        if(code <=0){
            return false;
        }
        
        long datapos = (code -1)*SIZE;
        rafData.seek(datapos);
        
        int i = rafData.readInt();
        String jsonAgenda = rafData.readUTF();
        
        rafData.seek(datapos);
        
        rafData.writeInt(i);
        rafData.writeUTF(jsonAgenda + "*");
        /*if(em.getNombre().substring(0, 1).equals("*")) 
        {
    
        }*/
        return true;
    }

    @Override
    public ArrayList<AgendaPojo> findAll() throws IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       open();
        
        List<AgendaPojo> agenda = new ArrayList<>();
        
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();
        
        for(int i = 0; i < n; i++){
            long posHead = 8 + i*4;
            rafHead.seek(posHead);
            
            int index = rafHead.readInt();
            long posData = (index - 1)* SIZE;
            rafData.seek(posData);
            
            String jsonAgenda = rafData.readUTF();
            if(jsonAgenda.substring(0,1).equals("*")){
                JOptionPane.showMessageDialog(null, "La informacion ha sido Eliminada");
                return null;
            }
            else{
                AgendaPojo a = gson.fromJson(jsonAgenda, AgendaPojo.class);
                agenda.add(a);
            }
        }
        close();
        return  (ArrayList<AgendaPojo>) agenda;
    }
}


