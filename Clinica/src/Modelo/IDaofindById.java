/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author Lenovo
 */
public interface IDaofindById extends IDao<AgendaPojo>{
    AgendaPojo findById(int id)throws IOException;
    AgendaPojo find(Comparator<AgendaPojo> fun) throws IOException;
    List<AgendaPojo> any(Predicate<AgendaPojo> fun) throws IOException;
}


