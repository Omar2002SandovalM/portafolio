/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Lenovo
 */
public class AgendaPojo {
    private int id;
    private String CodigoCita;
    private String nombre;
    private String segundoNombre;
    private String Apellido;
    private String SegundoApellido;
    private String fechaCita;
    private String horaCita;
    private String direccion;
    private String telefono;
    private String sexo;
    private String edad;
    private String estado; //si cancelo la cita o si  la pospuso
    
    //Constructor Sin Atributos

    public AgendaPojo() {
    }

    public AgendaPojo(String CodigoCita, String nombre, String segundoNombre, String Apellido, String SegundoApellido, String fechaCita, String horaCita, String direccion, String telefono, String sexo, String edad, String estado) {
        this.CodigoCita = CodigoCita;
        this.nombre = nombre;
        this.segundoNombre = segundoNombre;
        this.Apellido = Apellido;
        this.SegundoApellido = SegundoApellido;
        this.fechaCita = fechaCita;
        this.horaCita = horaCita;
        this.direccion = direccion;
        this.telefono = telefono;
        this.sexo = sexo;
        this.edad = edad;
        this.estado = estado;
    }

    public AgendaPojo(int id, String CodigoCita, String nombre, String segundoNombre, String Apellido, String SegundoApellido, String fechaCita, String horaCita, String direccion, String telefono, String sexo, String edad, String estado) {
        this.id = id;
        this.CodigoCita = CodigoCita;
        this.nombre = nombre;
        this.segundoNombre = segundoNombre;
        this.Apellido = Apellido;
        this.SegundoApellido = SegundoApellido;
        this.fechaCita = fechaCita;
        this.horaCita = horaCita;
        this.direccion = direccion;
        this.telefono = telefono;
        this.sexo = sexo;
        this.edad = edad;
        this.estado = estado;
    }

  
    //Metodos con los valores de los Atributos

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigoCita() {
        return CodigoCita;
    }

    public void setCodigoCita(String CodigoCita) {
        this.CodigoCita = CodigoCita;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getSegundoApellido() {
        return SegundoApellido;
    }

    public void setSegundoApellido(String SegundoApellido) {
        this.SegundoApellido = SegundoApellido;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}



