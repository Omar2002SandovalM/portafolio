/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author Lenovo
 */
public class Registro implements Serializable{
    private int id;
    private String ventas;
    private String CostosVariables;
    private String costosFijos;
    private String CapacidadNormal;
    private String Produccion;
    private String InventarioInicial;
    private String PrecioDeVentaUnitario;
    private String InventarioFinal;
    private String GastosVar;
    private String GastosFijos;
    //Constructor
    public Registro() {
    }

    public Registro(int id, String ventas, String CostosVariables, String costosFijos, String CapacidadNormal, String Produccion, String InventarioInicial, String PrecioDeVentaUnitario, String InventarioFinal, String GastosVar, String GastosFijos) {
        this.id = id;
        this.ventas = ventas;
        this.CostosVariables = CostosVariables;
        this.costosFijos = costosFijos;
        this.CapacidadNormal = CapacidadNormal;
        this.Produccion = Produccion;
        this.InventarioInicial = InventarioInicial;
        this.PrecioDeVentaUnitario = PrecioDeVentaUnitario;
        this.InventarioFinal = InventarioFinal;
        this.GastosVar = GastosVar;
        this.GastosFijos = GastosFijos;
    }
    
    public String getGastosVar() {
        return GastosVar;
    }

    public void setGastosVar(String GastosVar) {
        this.GastosVar = GastosVar;
    }

    public String getGastosFijos() {
        return GastosFijos;
    }

    public void setGastosFijos(String GastosFijos) {
        this.GastosFijos = GastosFijos;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVentas() {
        return ventas;
    }

    public void setVentas(String ventas) {
        this.ventas = ventas;
    }

    public String getCostosVariables() {
        return CostosVariables;
    }

    public void setCostosVariables(String CostosVariables) {
        this.CostosVariables = CostosVariables;
    }

    public String getCostosFijos() {
        return costosFijos;
    }

    public void setCostosFijos(String costosFijos) {
        this.costosFijos = costosFijos;
    }

    public String getCapacidadNormal() {
        return CapacidadNormal;
    }

    public void setCapacidadNormal(String CapacidadNormal) {
        this.CapacidadNormal = CapacidadNormal;
    }

    public String getProduccion() {
        return Produccion;
    }

    public void setProduccion(String Produccion) {
        this.Produccion = Produccion;
    }

    public String getInventarioInicial() {
        return InventarioInicial;
    }

    public void setInventarioInicial(String InventarioInicial) {
        this.InventarioInicial = InventarioInicial;
    }

    public String getPrecioDeVentaUnitario() {
        return PrecioDeVentaUnitario;
    }

    public void setPrecioDeVentaUnitario(String PrecioDeVentaUnitario) {
        this.PrecioDeVentaUnitario = PrecioDeVentaUnitario;
    }

    public String getInventarioFinal() {
        return InventarioFinal;
    }

    public void setInventarioFinal(String InventarioFinal) {
        this.InventarioFinal = InventarioFinal;
    }
}



































