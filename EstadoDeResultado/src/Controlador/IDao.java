/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Lenovo
 * @param <T>
 */
public interface IDao<T> {
   void create(T t)throws IOException;
   int update(T t)throws IOException;
   boolean delete(int id)throws IOException;
   ArrayList<T> findAll()throws IOException;
}










