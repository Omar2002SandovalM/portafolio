/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Registro;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lenovo
 */
public class RegistroIMP implements EstadoIDaoIMP{
    
    private RandomAccessFile Head;
    private RandomAccessFile Data;
    private File head;
    private File data;
    private final int SIZE = 80;
    
    
    private void open() throws IOException{
        head = new File("EstadoR_Head");
        data = new File("EstadoR_Data");
        if(!head.exists()){
            head.createNewFile();
            Head = new RandomAccessFile(head, "rw");
            Data = new RandomAccessFile(data, "rw");
            Head.seek(0);
            Head.writeInt(0);
            Head.writeInt(0);
        } else{
            Head = new RandomAccessFile(head, "rw");
            Data = new RandomAccessFile(data, "rw");
        }
    }
    
    private void close() throws IOException{
        if(Head != null){
            Head.close();
        }
        if(Data != null){
            Data.close();
        }
    }

    @Override
    public Registro findById(int id, DefaultTableModel model) throws IOException {
        open();
        Registro reg = null;
        
        Head.seek(0);
        int n = Head.readInt();
        int k = Head.readInt();
        
        if(id < 1 || id > k){
            return null;
        }
        
        int code = BinarySearch(id, 0, k);
        if(code < 0){
            return reg;
        }
        
        long posData = (code - 1) *SIZE;
        Data.seek(posData);
        int Id = Data.readInt();
        String Venta = Data.readUTF();
        String CostosV = Data.readUTF();
        String CostosF = Data.readUTF();
        String CapacidadN = Data.readUTF();
        String Produccion = Data.readUTF();
        String InventarioIn = Data.readUTF();
        String PrecioVenta = Data.readUTF();
        String InventarioFi = Data.readUTF();
        String GastosVar = Data.readUTF();
        String GastosFijos = Data.readUTF();
        
        if(Venta.substring(0, 1).equals("*")){
            JOptionPane.showMessageDialog(null, "La Informacion ha sido Eliminada");
            return null;
        }else{
            RellenoTable table = new RellenoTable();
            table.Rellenar(Id, Venta, CostosV, CostosF,CapacidadN,Produccion, InventarioIn, PrecioVenta, InventarioFi,GastosVar,GastosFijos, model);
        }
        close();
        return reg;
    }

     private int BinarySearch(int key, int low, int high) throws IOException {
        int middle = (low + high) / 2;
        if (high < low) {
            return -1;
        }
        long pos = 8 + 4 * middle;
        Head.seek(pos);
        int id = Head.readInt();
        if (key == id) {
            return id;
        } else if (key < id) {
            return BinarySearch(key, low, middle - 1);
        } else {
            return BinarySearch(key, middle + 1, high);
        }
    }
    @Override
    public void create(Registro t) throws IOException {
        open();
        
        Head.seek(0);
        int n = Head.readInt();
        int k = Head.readInt();
        
        long posdata = k *SIZE;
        Data.seek(posdata);
        
        Data.writeInt(k+1);
        Data.writeUTF(t.getVentas());
        Data.writeUTF(t.getCostosVariables());
        Data.writeUTF(t.getCostosFijos());
        Data.writeUTF(t.getCapacidadNormal());
        Data.writeUTF(t.getProduccion());
        Data.writeUTF(t.getInventarioInicial());
        Data.writeUTF(t.getPrecioDeVentaUnitario());
        Data.writeUTF(t.getInventarioFinal());
        Data.writeUTF(t.getGastosVar());
        Data.writeUTF(t.getGastosFijos());
        
        long posHeader = 8 + k*4;
        Head.seek(0);
        Head.writeInt(++n);
        Head.writeInt(++k);
        Head.seek(posHeader);
        Head.writeInt(k);
        
        close();
    }

    @Override
    public int update(Registro t) throws IOException {
        open();
        Head.seek(0);
        int n = Head.readInt();
        int k = t.getId();
        
        long posData = (k - 1)*SIZE;
        Data.seek(posData);
        
        if(t.getVentas().substring(0, 1).equals("*")){
            JOptionPane.showMessageDialog(null, "La Informacion ha sido Eliminada");
            return 1; //Error
        }else{
            Data.writeInt(k);
            Data.writeUTF(t.getVentas());
            Data.writeUTF(t.getCostosVariables());
            Data.writeUTF(t.getCostosFijos());
            Data.writeUTF(t.getCapacidadNormal());
            Data.writeUTF(t.getProduccion());
            Data.writeUTF(t.getInventarioInicial());
            Data.writeUTF(t.getPrecioDeVentaUnitario());
            Data.writeUTF(t.getInventarioFinal());
            Data.writeUTF(t.getGastosVar());
            Data.writeUTF(t.getGastosFijos());
        }
        close();
        return k;
    }

    @Override
    public boolean delete(int id) throws IOException {
        open();
        Head.seek(0);
        int n = Head.readInt();
        int k = Head.readInt();
        
        if(id < 1 || id > k){
            return false;
        }
        
        int code = BinarySearch(id, 0, k);
        if(code < 0){
            return false;
        }
        
        long posData = (code - 1) *SIZE;
        Data.seek(posData);
        
        int Id = Data.readInt();
        String Venta = Data.readUTF();
        String CostosV = Data.readUTF();
        String CostosF = Data.readUTF();
        String CapacidadN = Data.readUTF();
        String Produccion = Data.readUTF();
        String InventarioIn = Data.readUTF();
        String PrecioVenta = Data.readUTF();
        String InventarioFi = Data.readUTF();
        String GastosVar = Data.readUTF();
        String GastosFijos = Data.readUTF();
        
        Data.seek(posData);
        Data.writeInt(Id);
        Data.writeUTF("*"+Venta);
        Data.writeUTF(CostosV);
        Data.writeUTF(CostosF);
        Data.writeUTF(CapacidadN);
        Data.writeUTF(Produccion);
        Data.writeUTF(InventarioIn);
        Data.writeUTF(PrecioVenta);
        Data.writeUTF(InventarioFi);
        Data.writeUTF(GastosVar);
        Data.writeUTF(GastosFijos);
 
        close();
        return true;
    }

    @Override
    public ArrayList<Registro> findAll() throws IOException {
        open();
        List<Registro> cuentas = new ArrayList<>();
        Registro reg;
        
        Head.seek(0);
        int n = Head.readInt();
        int k = Head.readInt();
        
        for(int i = 0; i < n; i++){
            long posHeader =  8 + i*4;
            Head.seek(posHeader);
            int id = Head.readInt();
            
            long posData = (id -1)*SIZE;
            Data.seek(posData);
            
            int Id = Data.readInt();
            String Venta = Data.readUTF();
            String CostosV = Data.readUTF();
            String CostosF = Data.readUTF();
            String CapacidadN = Data.readUTF();
            String Produccion = Data.readUTF();
            String InventarioIn = Data.readUTF();
            String PrecioVenta = Data.readUTF();
            String InventarioFi = Data.readUTF();
            String GastosVar = Data.readUTF();
            String GastosFijos = Data.readUTF();
            
            reg = new Registro(Id, Venta, CostosV, CostosF, CapacidadN, Produccion, InventarioIn, PrecioVenta, InventarioFi,GastosVar,GastosFijos);
            cuentas.add(reg);
        }
        close();
        return (ArrayList<Registro>) cuentas;
    }
    
    //---------------------------------------------------Operaciones del Costeo Absorvente------------------------------------------------------------//
    
    public double IngresoxVenta(double Ventas, double PV){
        return Ventas*PV;
    }
    
    public double CostoDeVenta(double Cv, double Cf, double CapacidadNormal,double InventarioInicial,double Produccion,double InventarioFinal){
        double costoUnitario = ((Cv +Cf)/CapacidadNormal);
        double CostoUnitarioFijo =(Cf/Produccion);
        double CapacidadNoFabril = (CapacidadNormal - Produccion);
        
        double CostoVenta = ((InventarioInicial * costoUnitario)+(Produccion * costoUnitario)-(InventarioFinal * costoUnitario)+(CapacidadNoFabril*CostoUnitarioFijo));
        return CostoVenta;
    }
    
    public double UtilidadBruta(double Ventas, double PrecioVenta, double CV, double CF, double CapacidadNormal, double InvIni, double Produccion,double InvFinal){
        return IngresoxVenta(Ventas, PrecioVenta)-CostoDeVenta(CV, CF, CapacidadNormal,InvIni, Produccion, InvFinal);
    }
    
    public double GastosOperacionales(double GastosVar, double GastosFijos){
        return GastosVar + GastosFijos;
    }
    public double UtilidadAntesDeImpuestos(double GastosVar, double GastosFijos,double Ventas, double PrecioVenta, double CV, double CF, double CapacidadNormal, double InvIni, double Produccion,double InvFinal){
        double UAI =  UtilidadBruta(Ventas, PrecioVenta, CV, CF, CapacidadNormal, InvIni, Produccion, InvFinal) - GastosOperacionales(GastosVar, GastosFijos);
        return UAI;
    }
    
    public double IR(double GastosVar, double GastosFijos,double Ventas, double PrecioVenta, double CV, double CF, double CapacidadNormal, double InvIni, double Produccion,double InvFinal){
        return 0.30 * UtilidadAntesDeImpuestos(GastosVar, GastosFijos, Ventas, PrecioVenta, CV, CF, CapacidadNormal, InvIni, Produccion, InvFinal);
    }
    
    public double UtilidadDespuesDeImpuestos(double GastosVar, double GastosFijos,double Ventas, double PrecioVenta, double CV, double CF, double CapacidadNormal, double InvIni, double Produccion,double InvFinal){
        return UtilidadAntesDeImpuestos(GastosVar, GastosFijos, Ventas, PrecioVenta, CV, CF, CapacidadNormal, InvIni, Produccion, InvFinal) - IR(GastosVar, GastosFijos, Ventas, PrecioVenta, CV, CF, CapacidadNormal, InvIni, Produccion, InvFinal);
    }
    
    /*--------------------------------------------------Metodos Costeo Directo---------------------------------------------------------*/
    public double IngresoxVentaDirecto(double Venta, double PV){
        return Venta*PV;
    }
}//Final de la clase












































































































