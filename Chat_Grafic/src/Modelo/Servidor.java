/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import com.sun.glass.ui.Window;
import java.awt.HeadlessException;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Lenovo
 */
public class Servidor extends Observable implements Runnable{
    private final int puerto;

    public Servidor(int puerto) {
        this.puerto = puerto;
    }
    
    @Override
    public void run() {
        //Creamos Objetos
        ServerSocket servidor = null;
        Socket sc = null;
        DataInputStream in;
        
        try {
            //Creamos el socket del servidor
            servidor = new ServerSocket(puerto);
            JOptionPane.showMessageDialog(null,"Servidor Iniciado");
            
            //Siempre estara escuchando Peticiones
            while (true){
                //Espera a que un cliente se conecte
                sc = servidor.accept();
                JOptionPane.showMessageDialog(null, "Cliente Conectado");
                in = new DataInputStream(sc.getInputStream());
                
                //Leo el mensaje que me Envia el Cliente
                
                String mensaje = in.readUTF();
                JOptionPane.showMessageDialog(null, mensaje);
                
                this.setChanged();
                this.notifyObservers(mensaje);
                this.clearChanged();
                
                //Cierro el socket
                sc.close();
                JOptionPane.showMessageDialog(null, "Cliente Desconectado...");
            }
        } catch (IOException e) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}



























