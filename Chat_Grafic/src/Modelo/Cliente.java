/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Cliente implements Runnable{

    private final int Puerto;
    private final String mensaje;

    public Cliente(int Puerto, String mensaje) {
        this.Puerto = Puerto;
        this.mensaje = mensaje;
    }
    
    @Override
    public void run() {
        //Host del Servidor
        final String HOST = "127.0.0.1";
        //Puerto del Servidor
        DataOutputStream out;
        
        try {
            //Creo el socket para conectarme con el cliente
            Socket s = new Socket(HOST,Puerto);
            out = new DataOutputStream(s.getOutputStream());
            //Envio un mensaje al cliente
            out.writeUTF(mensaje);
            s.close();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}














