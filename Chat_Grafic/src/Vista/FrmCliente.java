/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Cliente;
import Modelo.Servidor;
import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Lenovo
 */
public class FrmCliente extends javax.swing.JFrame implements Observer{
    
    public FrmCliente() {
        initComponents();
        btnDeleteMessage1.setVisible(false);
        
        this.getRootPane().setDefaultButton(this.btnEnviar);
        Servidor s = new Servidor(6000);
        s.addObserver(this);
        Thread t = new Thread(s);
        t.start();
    }

    @Override
    public void update(Observable o, Object arg) {
        this.AreaMsg1.append((String) arg);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnMenuDeslizable = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        AreaMsg = new javax.swing.JTextArea();
        pnlContent = new javax.swing.JPanel();
        txtMessage = new javax.swing.JTextField();
        btnDeleteMessage = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnSend = new javax.swing.JButton();
        PnlBarra = new javax.swing.JPanel();
        lblCerrar = new javax.swing.JLabel();
        lblMinimizar = new javax.swing.JLabel();
        pnlContent1 = new javax.swing.JPanel();
        txtMensaje = new javax.swing.JTextField();
        btnDeleteMessage1 = new javax.swing.JButton();
        SeparatorMessage = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        AreaMsg1 = new javax.swing.JTextArea();
        btnEnviar = new javax.swing.JButton();
        PnlBarra3 = new javax.swing.JPanel();
        lblClose = new javax.swing.JLabel();
        lblMinimizar1 = new javax.swing.JLabel();

        btnMenuDeslizable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/menu.png"))); // NOI18N
        btnMenuDeslizable.setBorder(null);
        btnMenuDeslizable.setContentAreaFilled(false);
        btnMenuDeslizable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnMenuDeslizable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuDeslizableActionPerformed(evt);
            }
        });

        AreaMsg.setColumns(20);
        AreaMsg.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        AreaMsg.setRows(5);
        AreaMsg.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(232, 69, 69), 2));
        jScrollPane1.setViewportView(AreaMsg);

        pnlContent.setBackground(new java.awt.Color(255, 255, 255));
        pnlContent.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 0));
        pnlContent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlContentMouseClicked(evt);
            }
        });
        pnlContent.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtMessage.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        txtMessage.setToolTipText("Escribiendo Mensaje");
        txtMessage.setBorder(null);
        txtMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMessageActionPerformed(evt);
            }
        });
        pnlContent.add(txtMessage, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 570, 470, 40));

        btnDeleteMessage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/delete.png"))); // NOI18N
        btnDeleteMessage.setToolTipText("Borrar Todo el Mensaje");
        btnDeleteMessage.setContentAreaFilled(false);
        btnDeleteMessage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnDeleteMessageMouseReleased(evt);
            }
        });
        btnDeleteMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteMessageActionPerformed(evt);
            }
        });
        pnlContent.add(btnDeleteMessage, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 570, 40, 40));

        jSeparator1.setBackground(new java.awt.Color(232, 69, 69));
        jSeparator1.setForeground(new java.awt.Color(232, 69, 69));
        pnlContent.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 610, 470, 40));

        btnSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/send.png"))); // NOI18N
        btnSend.setToolTipText("Enviar");
        btnSend.setContentAreaFilled(false);
        pnlContent.add(btnSend, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 570, 40, 40));

        PnlBarra.setBackground(new java.awt.Color(43, 46, 74));
        PnlBarra.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                PnlBarraMouseDragged(evt);
            }
        });
        PnlBarra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                PnlBarraMousePressed(evt);
            }
        });
        PnlBarra.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/error.png"))); // NOI18N
        lblCerrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCerrarMouseClicked(evt);
            }
        });
        PnlBarra.add(lblCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, -1, -1));

        lblMinimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/inside.png"))); // NOI18N
        lblMinimizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblMinimizarMouseClicked(evt);
            }
        });
        PnlBarra.add(lblMinimizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, -33, -1, 30));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Chat_Servidor");
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlContent1.setBackground(new java.awt.Color(255, 255, 255));
        pnlContent1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cliente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri Light", 0, 24))); // NOI18N
        pnlContent1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlContent1MouseClicked(evt);
            }
        });
        pnlContent1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtMensaje.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        txtMensaje.setToolTipText("Escribiendo Mensaje");
        txtMensaje.setBorder(null);
        txtMensaje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtMensajeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtMensajeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtMensajeMouseExited(evt);
            }
        });
        txtMensaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMensajeActionPerformed(evt);
            }
        });
        pnlContent1.add(txtMensaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 570, 470, 40));

        btnDeleteMessage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/delete.png"))); // NOI18N
        btnDeleteMessage1.setToolTipText("Borrar Todo el Mensaje");
        btnDeleteMessage1.setBorder(null);
        btnDeleteMessage1.setContentAreaFilled(false);
        btnDeleteMessage1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnDeleteMessage1MouseReleased(evt);
            }
        });
        btnDeleteMessage1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteMessage1ActionPerformed(evt);
            }
        });
        pnlContent1.add(btnDeleteMessage1, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 630, 30, -1));

        SeparatorMessage.setBackground(new java.awt.Color(232, 69, 69));
        SeparatorMessage.setForeground(new java.awt.Color(232, 69, 69));
        pnlContent1.add(SeparatorMessage, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 610, 470, 20));

        AreaMsg1.setColumns(20);
        AreaMsg1.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        AreaMsg1.setRows(5);
        AreaMsg1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(43, 46, 74), 2));
        jScrollPane2.setViewportView(AreaMsg1);

        pnlContent1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 470, 530));

        btnEnviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/send.png"))); // NOI18N
        btnEnviar.setToolTipText("Enviar");
        btnEnviar.setBorder(null);
        btnEnviar.setContentAreaFilled(false);
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });
        pnlContent1.add(btnEnviar, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 630, 40, 40));

        getContentPane().add(pnlContent1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 520, 680));

        PnlBarra3.setBackground(new java.awt.Color(43, 46, 74));
        PnlBarra3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                PnlBarra3MouseDragged(evt);
            }
        });
        PnlBarra3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                PnlBarra3MousePressed(evt);
            }
        });
        PnlBarra3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/error.png"))); // NOI18N
        lblClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCloseMouseClicked(evt);
            }
        });
        PnlBarra3.add(lblClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 10, -1, -1));

        lblMinimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/inside.png"))); // NOI18N
        lblMinimizar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblMinimizar1MouseClicked(evt);
            }
        });
        PnlBarra3.add(lblMinimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, -33, -1, 30));

        getContentPane().add(PnlBarra3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 50));

        setSize(new java.awt.Dimension(519, 728));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void pnlContentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlContentMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_pnlContentMouseClicked

    private void PnlBarraMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PnlBarraMousePressed

    }//GEN-LAST:event_PnlBarraMousePressed

    private void PnlBarraMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PnlBarraMouseDragged

    }//GEN-LAST:event_PnlBarraMouseDragged

    private void txtMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMessageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMessageActionPerformed

    private void btnDeleteMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteMessageActionPerformed
        // TODO add your handling code here:
        txtMessage.setText(null);
    }//GEN-LAST:event_btnDeleteMessageActionPerformed

    private void btnDeleteMessageMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMessageMouseReleased
      
    }//GEN-LAST:event_btnDeleteMessageMouseReleased

    private void lblCerrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCerrarMouseClicked
       dispose();
    }//GEN-LAST:event_lblCerrarMouseClicked

    private void lblMinimizarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMinimizarMouseClicked
        // TODO add your handling code here:
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_lblMinimizarMouseClicked

    private void btnMenuDeslizableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuDeslizableActionPerformed
       
    }//GEN-LAST:event_btnMenuDeslizableActionPerformed

    private void txtMensajeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtMensajeMouseClicked
        btnDeleteMessage1.setVisible(true);
    }//GEN-LAST:event_txtMensajeMouseClicked

    private void txtMensajeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtMensajeMouseEntered
        // TODO add your handling code here:
        SeparatorMessage.setBackground(new Color(43,46,74));
        SeparatorMessage.setForeground(new Color(43,46,74));
    }//GEN-LAST:event_txtMensajeMouseEntered

    private void txtMensajeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtMensajeMouseExited
        SeparatorMessage.setBackground(new Color(232,69,69));
        SeparatorMessage.setForeground(new Color(232,69,69));
    }//GEN-LAST:event_txtMensajeMouseExited

    private void txtMensajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMensajeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMensajeActionPerformed

    private void btnDeleteMessage1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMessage1MouseReleased

    }//GEN-LAST:event_btnDeleteMessage1MouseReleased

    private void btnDeleteMessage1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteMessage1ActionPerformed
        // TODO add your handling code here:
        txtMensaje.setText(null);
    }//GEN-LAST:event_btnDeleteMessage1ActionPerformed

    private void pnlContent1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlContent1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_pnlContent1MouseClicked

    private void lblCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCloseMouseClicked
        dispose();
    }//GEN-LAST:event_lblCloseMouseClicked

    private void lblMinimizar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMinimizar1MouseClicked
        // TODO add your handling code here:
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_lblMinimizar1MouseClicked
    int xx,xy;
    private void PnlBarra3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PnlBarra3MouseDragged
        // TODO add your handling code here:4
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();

        this.setLocation(x-xx, y-xy);
    }//GEN-LAST:event_PnlBarra3MouseDragged

    private void PnlBarra3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PnlBarra3MousePressed
        // TODO add your handling code here:
        xx = evt.getX();
        xy = evt.getY();
    }//GEN-LAST:event_PnlBarra3MousePressed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        // TODO add your handling code here:
        String mensaje = "Cliente:"+this.txtMensaje.getText() + "\n";
        this.AreaMsg1.append(mensaje);
        Cliente c = new Cliente(5000,mensaje);
        Thread t = new Thread(c);
        t.start();
    }//GEN-LAST:event_btnEnviarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea AreaMsg;
    private javax.swing.JTextArea AreaMsg1;
    private javax.swing.JPanel PnlBarra;
    private javax.swing.JPanel PnlBarra3;
    private javax.swing.JSeparator SeparatorMessage;
    private javax.swing.JButton btnDeleteMessage;
    private javax.swing.JButton btnDeleteMessage1;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton btnMenuDeslizable;
    private javax.swing.JButton btnSend;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblCerrar;
    private javax.swing.JLabel lblClose;
    private javax.swing.JLabel lblMinimizar;
    private javax.swing.JLabel lblMinimizar1;
    private javax.swing.JPanel pnlContent;
    private javax.swing.JPanel pnlContent1;
    private javax.swing.JTextField txtMensaje;
    private javax.swing.JTextField txtMessage;
    // End of variables declaration//GEN-END:variables

  
}

