/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sincronizaciondehilosvideo3;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class SincronizacionDeHilosVideo3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Banco b = new Banco();
        
        for(int i =0 ; i < 100; i++){
            EjecucionTransferencia r = new EjecucionTransferencia(b, i, 2000);
            
            Thread t = new Thread(r);
            
            t.start();
        }
    }
}

class Banco{

    public Banco() {
        cuentas = new double[100];
        
        for(int i =0; i < cuentas.length; i++){
            cuentas[i] = 2000;
        }
    }
    
    public void transferencia(int cuentaOrigen,int CuentaDestino, double Cantidad ){
        
        cierreBanco.lock();
        
        try{
        
            if(cuentas[cuentaOrigen] < Cantidad){//Evalua que el saldo no es menor  a la transferencia
                System.out.println("---------------------CANTIDAD INSUFICIENTE:"+cuentaOrigen + "--------SALDO:"+cuentas[cuentaOrigen]+"------------"+Cantidad);
                return;
            } else {
                System.out.println("---------------------CANTIDAD OK-------"+ cuentaOrigen+"------------SALDO:"+cuentas[cuentaOrigen]+"-----------"+Cantidad);
            }

            System.out.println(Thread.currentThread());

            cuentas[cuentaOrigen] -=Cantidad; // dinero que sale de la cuenta Origen

            System.out.printf("%10.2f de %d para %d:", Cantidad,cuentaOrigen,CuentaDestino);

            cuentas[CuentaDestino] +=Cantidad; // Aumentar la misama cantiddad mas lo que se deposito

            System.out.printf("\nSaldo Total: %10.2f%n:", getSaldoTotal());

        } finally{
            cierreBanco.unlock();
        }
    }    
    
    
    public double getSaldoTotal(){
        double suma_Cuentas = 0;
        
        for(double a : cuentas){
            suma_Cuentas+=a;
        }
        return suma_Cuentas;
    }
    
    private final double[] cuentas;
    
    private Lock cierreBanco = new ReentrantLock();
}

class EjecucionTransferencia implements Runnable{
    public EjecucionTransferencia(Banco b, int de, double max) {
        banco = b;
        delaCuenta = de;
        cantidadMax = max;
    }
    
    @Override
    public void run() {
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        while(true){
            try {
                int paraLaCuenta = (int) (100 * Math.random());
                
                double cantidad = cantidadMax * Math.random();
                
                banco.transferencia(delaCuenta, paraLaCuenta, cantidad);
                
                Thread.sleep((int)(Math.random() * 10));
            } catch (InterruptedException ex) {
                Logger.getLogger(EjecucionTransferencia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private  Banco banco;
    private int delaCuenta;
    private double cantidadMax;
}















































