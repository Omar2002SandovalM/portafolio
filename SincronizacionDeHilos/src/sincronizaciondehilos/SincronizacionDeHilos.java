/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sincronizaciondehilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class SincronizacionDeHilos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HilosVario hilo1 = new HilosVario();
        HilosVarios hilo2 = new HilosVarios(hilo1);
        
        hilo1.start();
        hilo2.start();
        
        System.out.println("Tareas Terminadas");
    }
}

class PrimerVideoDeSincronizacion{
    
    public static void Arranca(){
        // TODO code application logic here
        HilosVario hilo1 = new HilosVario();
        
        HilosVario hilo2 = new HilosVario();
        
        hilo1.start();
        try {
            hilo1.join(); //Obliga a que se Ejecute este Hilo Primero pero primero debe Terminar todo su prosceso
        } catch (InterruptedException ex) {
            Logger.getLogger(SincronizacionDeHilos.class.getName()).log(Level.SEVERE, null, ex);
        }
        hilo2.start();
        
        try {
            hilo2.join();//Luego se Ejecutara este Hilo
        } catch (InterruptedException ex) {
            Logger.getLogger(SincronizacionDeHilos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Terminada la tareas");//Cundo termine el Hilo dos se Imprimira esto y luego comenzara el hilo 1
        //Al sincronizar los hilos e iniciar uno y al terminar el otro, el sout se imprimira al finals
        //Al aplicar el punto join a ambos hilos logramos Sincronizarlos
    
    }
}


class HilosVario extends Thread{
    @Override
    public void run(){
        for(int i =0; i < 15; i++){
            try {
                System.out.println("Ejecutando el Hilo" + getName());//getName devuelve el nombre del Hilo que esta ejecutando la accion
                Thread.sleep(700);
            } catch (InterruptedException ex) {
                Logger.getLogger(HilosVario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
class HilosVarios extends Thread{

    public HilosVarios(Thread hilo) {
        this.hilo = hilo;
    }
    
    @Override
    public void run(){
        
        try {
            hilo.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(HilosVarios.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(int i =0; i < 15; i++){
            try {
                System.out.println("Ejecutando el Hilo" + getName());//getName devuelve el nombre del Hilo que esta ejecutando la accion
                Thread.sleep(700);
            } catch (InterruptedException ex) {
                Logger.getLogger(HilosVario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private Thread hilo;
}















